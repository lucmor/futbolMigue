wN=[0m
G=[01;32m
Y=[01;33m
B=[01;34m

comandos:
	@echo "${B}Los comandos disponibles son:${N} "
	@echo "    instalar_dependencias               Instala las dependencias necesarias para el proyecto"
	@echo "    electron                            Inicia electron con el proyecto"
	@echo "    electron_live                       Inicia electron en modo live"
	@echo "    compilar                            Compila la aplicacion para generar la pagina estatica"
	@echo "    empaquetar                          Empaqueta la pagina estatica con electron"
	@echo "    arreglo_policies                    Arregla el error de policies"	
	@echo "    arreglo_parser                      Arregla el parser de vue-loader"

instalar_dependencias:
	@echo "${B}Instalando dependencias...${N}"
	@yarn install
	@make arreglo_policies
	@make arreglo_parser
	@make copiar_libs

copiar_libs:
	@cp node_modules/dexie/dist/dexie.min.js static/js/typescript/dexie.js
	@cp node_modules/dexie/dist/dexie.d.ts typescript/dexie.d.ts

electron:
	@echo "${B}Iniciando electron...${N}"
	@yarn run start

electron_live:
	@echo "${B}Iniciando electron en tiempo real...${N}"
	@yarn run dev

compilar:
	@echo "${B}Compilando...${N}"
	@yarn run build

empaquetar:
	@echo "${B}Empaquetando...${N}"
	@yarn run pack

arreglo_policies:
ifeq ($(OS),Windows_NT)
	@echo "Ejecute el siguiente comando"
	@echo "parches\sed.exe -i \"s/const policies = /const policies = null \/\//\" node_modules\nuxt\lib\core\middleware\nuxt.js"
else
	@echo "${B}Aplicando parche...${N}"
	@sed -i 's/const policies = /const policies = null \/\//g' node_modules/nuxt/lib/core/middleware/nuxt.js
endif

arreglo_parser:
ifeq ($(OS),Windows_NT)
	@echo "Ejecute el siguiente comando"
	@echo "parches\sed.exe -i \"s/semi: false/semi: false, parser: \"babylon\"/g\" node/modules\vue-loader\lib\template-compiles\index.js"
else
	@echo "${B}Aplicando parche...${N}"
	@sed -i 's/semi: false/semi: false, parser: "babylon"/g' node_modules/vue-loader/lib/template-compiler/index.js
endif

