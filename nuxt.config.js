module.exports = {
    build: {
        extend (config, { isClient }) {
            // Extend only webpack config for client-bundle
            if (isClient) {
                config.target = 'electron-renderer'
            }
        }
    },
    head: {
		script: [
			{ src: 'https://use.fontawesome.com/releases/v5.0.9/js/all.js' },
            //{ src: 'http://localhost:8098'},
            //{ src: '/js/db.js' },
            /*{ src: '/js/dbN.js' },
            { src: '/js/jugador.js' },
            { src: '/js/equipo.js' },
            { src: '/js/futbol.js' }*/
		],
        link: [
            { rel: 'stylesheet', href: 'https://meyerweb.com/eric/tools/css/reset/reset.css' },
			{ rel: 'stylesheet', href: '/css/base.css' }
        ]
    }
}
