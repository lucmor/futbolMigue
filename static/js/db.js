var sqlite3 = require('sqlite3').verbose()
var db = new sqlite3.Database('data.db')

db.run("CREATE TABLE IF NOT EXISTS equipos(\
    id INTEGER PRIMARY KEY AUTOINCREMENT,\
    nombre TEXT,\
    jugadores TEXT NOT NULL DEFAULT -1,\
    pj INTEGER NOT NULL DEFAULT 0,\
    pg INTEGER NOT NULL DEFAULT 0,\
    pe INTEGER NOT NULL DEFAULT 0,\
    pp INTEGER NOT NULL DEFAULT 0,\
    gf INTEGER NOT NULL DEFAULT 0,\
    gc INTEGER NOT NULL DEFAULT 0);")

db.run("CREATE TABLE IF NOT EXISTS jugadores(\
    id INTEGER PRIMARY KEY AUTOINCREMENT,\
    nombre TEXT,\
    documento INTEGER,\
    fechanac TEXT,\
    domicilio TEXT);")

function cargarEquipos(equipos) {
	var stmt = db.prepare("SELECT id, nombre FROM equipos")

	stmt.all(function(error, rows){
		if (error) {
			throw error
		} else {
			stmt.finalize()

			rows.forEach((row) => {
                equipos.push(row)
            })
		}
	})
}

function agregarEquipo(nombre) {
    var stmt = db.prepare("INSERT INTO equipos (nombre) VALUES (?)")

    stmt.run(nombre)

    stmt.finalize()
}

function modificarEquipo(id, nombreN) {
    var stmt = db.prepare("UPDATE equipos SET nombre = ? WHERE id = ?")

    stmt.run(nombreN, id)

    stmt.finalize()
}

function eliminarEquipo(id) {
    var stmt = db.prepare("DELETE FROM equipos WHERE id = ?")

    stmt.run(id)

    stmt.finalize()
}

function agregarJugador(idEquipo, nombre, documento, fecha, domicilio, dataEquipo) {
    var stmt = db.prepare("INSERT INTO jugadores (nombre, documento, fechanac, domicilio) VALUES (?, ?, ?, ?)")

    stmt.run(nombre, documento, fecha, domicilio)

    stmt.finalize()

    stmt = db.prepare("SELECT LAST_INSERT_ROWID()")

    stmt.get(function(error, row){
        if (error) {
            throw error
        } else {
            stmt.finalize()

            stmt = db.prepare("UPDATE equipos SET jugadores = jugadores || '," + row["LAST_INSERT_ROWID()"] + "' WHERE id = ?")
            stmt.run(idEquipo)

            stmt.finalize()

            cargarJugadores(idEquipo, dataEquipo)
        }
    })
}

function modificarJugador(id, column, nuevoDato, idEquipo, dataEquipo) {
    var stmt = db.prepare("UPDATE jugadores SET " + column + " = ? WHERE id = ?")

    stmt.run(nuevoDato, id)

    stmt.finalize()

    cargarJugadores(idEquipo, dataEquipo)
}

function cargarJugadores(id, jugadores) {
    let stmt = db.prepare("SELECT jugadores FROM equipos WHERE id = ?")

    stmt.bind(id)

    stmt.get(function(error, row){
        if (error) {
             throw error
        } else {
            stmt.finalize()

            if (row.jugadores == null) {
                equipos = []
            } else {
                let data_parentesis = "(" + row.jugadores.substring(0, 3) + ")"

                let stmt_2 = db.prepare("SELECT * FROM jugadores WHERE id IN " + data_parentesis)

                stmt_2.all(function(error_, rows){
                    if (error_) {
                        throw error_
                    } else {
                        rows.forEach((row_) => {
                            var data_jugador = {}

                            data_jugador["id"] = row_.id
                            data_jugador["nombre"] = row_.nombre
                            data_jugador["dni"] = row_.documento
                            data_jugador["fecha"] = row_.fechanac
                            data_jugador["domicilio"] = row_.domicilio

                            jugadores.push(data_jugador)
                        })
                    }
                })
            }
        }
    })
}

function cargarPosiciones(posiciones_equipos) {
    let stmt = db.prepare("SELECT nombre, pj, pg, pe, pp, gf, gc FROM equipos")

    stmt.all(function(error, rows){
        if (error) {
            throw error
        } else {
            rows.forEach((row) => {
                let data_equipo = {}

                data_equipo["nombre"] = row.nombre
                data_equipo["pj"] = row.pj
                data_equipo["pg"] = row.pg
                data_equipo["pe"] = row.pe
                data_equipo["pp"] = row.pp
                data_equipo["gf"] = row.gf
                data_equipo["gc"] = row.gc
                data_equipo["dif"] = row.gf - row.gc
                data_equipo["puntos"] = row.pg * 2 + row.pe

                posiciones_equipos.push(data_equipo)
            })
        }
    })
}
