//var sqlite3 = require('sqlite3').verbose()
var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB

var DB_NAME = "dataFutbol"
var DB_VERSION = 1


class DB {
    constructor() {
        this.db = null
        this.init()
    }

    async init() {
        let this_ = this

        let request = await indexedDB.open(DB_NAME, DB_VERSION)

        request.onsuccess = function() {
            this_.db = request.result

            this_.db.onerror = function(event) {
                console.log("Error en la base de datos: " + event.target.errorCode)
            }
        }

        request.onerror = function(event) {
            console.log("No se pudo abrir la db " + event)
        }

        request.onupgradeneeded = function() {
            this_.db = request.result

            let equiposStore = this_.db.createObjectStore("equipos", { keyPath: "id", autoIncrement: true })
            let jugadoresStore = this_.db.createObjectStore("jugadores", { keyPath: "id", autoIncrement: true })

            equiposStore.createIndex("id", "id", { unique: true })

            jugadoresStore.createIndex("id", "id", { unique: true })
        }
    }

    todosEquipos(callback, equipos, jugadores) {
        let transaction = this.db.transaction("equipos", "readonly")

        let objectStore = transaction.objectStore("equipos")

        let request = objectStore.getAll()

        request.onsuccess = function(event) {
            let data = request.result

            callback(data, equipos, jugadores)
        }
    }

    todosJugadores(callback, jugadores, callbackDB, cargaEquipos, equipos   ) {
        let transaction = this.db.transaction("jugadores", "readonly")

        let objectStore = transaction.objectStore("jugadores")

        let request = objectStore.getAll()

        request.onsuccess = function(event) {
            let data = request.result

            callback(data, jugadores)
        }
    }

    guardarJugador(jugador, jugadores, idEquipo = 0, callbackAgregarAEquipo = () => {}, equipos = {}, callbackCargarJugadores = () => {}, dataJugadores = []) {
        let transaction = this.db.transaction("jugadores", "readwrite")

        let objectStore = transaction.objectStore("jugadores")

        if (jugador.getId() == 0) {
            let data = {}

            data["nombre"] = jugador.getNombre()
            data["documento"] = jugador.getDocumento()
            data["fechanac"] = jugador.getFechaNac()
            data["domicilio"] = jugador.getDomicilio()

            let request = objectStore.add(data)
            let this_ = this

            request.onsuccess = function(event) {
                let request_2 = objectStore.getAll()

                request_2.onsuccess = function(event) {
                    let data_2 = request_2.result

                    let id = data_2[data_2.length - 1].id

                    jugador.setId(id)

                    jugadores[id] = jugador


                    callbackAgregarAEquipo(jugador, idEquipo, equipos, callbackCargarJugadores, dataJugadores, this_)
                }
            }
        } else {
            let request = objectStore.get(jugador.getId())

            request.onsuccess = function(event) {
                let data = request.result

                data.nombre = jugador.getNombre()
                data.documento = jugador.getDocumento()
                data.fechanac = jugador.getFechaNac()
                data.domicilio = jugador.getDomicilio()

                let request_update = objectStore.put(data)

                callbackCargarJugadores(dataJugadores, jugadores)
            }
        }
    }

    guardarEquipo(equipo, equipos = [], callbackCargarEquipos = () => {}, dataEquipo = []) {
        let transaction = this.db.transaction("equipos", "readwrite")

        let objectStore = transaction.objectStore("equipos")

        if (equipo.getId() == 0) {
            let data = {}

            data["nombre"] = equipo.getNombre()
            data["jugadores"] = Object.keys(equipo.getJugadores()).join()
            data["pj"] = equipo.getPj()
            data["pg"] = equipo.getPg()
            data["pe"] = equipo.getPe()
            data["pp"] = equipo.getPp()
            data["gf"] = equipo.getGf()
            data["gc"] = equipo.getGc()

            let request = objectStore.add(data)

            request.onsuccess = function(event) {
                let request_2 = objectStore.getAll()

                request_2.onsuccess = function(event) {
                    let data_2 = request_2.result

                    let id = data_2[data_2.length - 1].id

                    equipo.setId(id)

                    equipos[id] = equipo

                    callbackCargarEquipos(dataEquipo, equipos)
                }
            }
        } else { 
            let request = objectStore.get(equipo.getId())

            request.onsuccess = function(event) {
                let data = request.result
                
                data.nombre = equipo.getNombre()
                data.jugadores = Object.keys(equipo.getJugadores()).join()
                data.pj = equipo.getPj()
                data.pg = equipo.getPg()
                data.pe = equipo.getPe()
                data.pp = equipo.getPp()
                data.gf = equipo.getGf()
                data.gc = equipo.getGc()

                let request_update = objectStore.put(data)
            }
        }
    }

    eliminarEquipo(id) {
        let transaction = this.db.transaction("equipos", "readwrite")

        let objectStore = transaction.objectStore("equipos")

        objectStore.delete(id)
    }
}
