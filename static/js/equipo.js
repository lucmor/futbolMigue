class Equipo {
	constructor(nombre, id = 0) {
		this.id = id
		this.nombre = nombre
		this.jugadores = {}
		this.pj = 0
		this.pg = 0
		this.pe = 0
		this.pp = 0
		this.gf = 0
		this.gc = 0
	}

	getGc() {
		return this.gc
	}

	getGf() {
		return this.gf
	}

	getId() {
		return this.id
	}

	getJugadores() {
		return this.jugadores
	}

	getNombre() {
		return this.nombre
	}

	getPj() {
		return this.pj
	}

	getPg() {
		return this.pg
	}

	getPe() {
		return this.pe
	}

	getPp() {
		return this.pp
	}

	setGc(gc) {
		this.gc = gc
	}

	setGf(gf) {
		this.gf = gf
	}

	setId(id) {
		this.id = id
	}

	setNombre(nombre) {
		this.nombre = nombre
	}

	setPj(pj) {
		this.pj = pj
	}

	setPg(pg) {
		this.pg = pg
	}

	setPe(pe) {
		this.pe = pe
	}

	setPp(pp) {
		this.pp = pp
	}

	guardar(db, equipos = [], callbackCargarEquipos = () => {}, dataEquipo = []) {
		db.guardarEquipo(this, equipos, callbackCargarEquipos, dataEquipo)
	}
}
