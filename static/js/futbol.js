class Futbol {
	constructor() {
		this.jugadores = {}
		this.equipos = {}
		this.jugadores_cargados = false
		this.equipos_cargados = false
		this.db = new DB()

		setTimeout(this.cargarDatos, 1000, this.db, this)
	}

	getJugadores() {
		return this.jugadores
	}

	getEquipos() {
		return this.equipos
	}

	getJugador(id) {
		return this.jugadores[id]
	}

	getEquipo(id) {
		return this.equipos[id]
	}

	agregarJugador(id, nombre, documento, fecha, domicilio, dataJugadores) {
		let jugador = new Jugador(nombre)

		jugador.setDocumento(documento)
		jugador.setFechaNac(fecha)
		jugador.setDomicilio(domicilio)

		jugador.guardar(this.db, this.jugadores, id, this.guardarEnEquipo, this.equipos, this.cargarJugadoresA, dataJugadores)
	}

	modificarJugador(id, dato, datoNuevo, idEquipo, dataJugadores) {
		let jugador = this.jugadores[id]

		switch (dato) {
			case "nombre":
				jugador.setNombre(datoNuevo)
				break
			case "documento":
				jugador.setDocumento(datoNuevo)
				break
			case "fecha":
				jugador.setFechaNac(datoNuevo)
				break
			case "domicilio":
				jugador.setDomicilio(datoNuevo)
				break
		}

		jugador.guardar(this.db, this.jugadores, 0, () => {}, [], this.cargarJugadoresA, dataJugadores)
	}

	agregarEquipo(nombre, dataEquipo) {
		let equipo = new Equipo(nombre)

		equipo.guardar(this.db, this.equipos, this.cargarEquiposA, dataEquipo)
	}

	modificarEquipo(id, nuevo, dataEquipo) {
		this.equipos[parseInt(id)].setNombre(nuevo)

		this.equipos[parseInt(id)].guardar(this.db)

		this.cargarEquiposA(dataEquipo, this.equipos)
	}

	eliminarEquipo(id, dataEquipo) {
		delete this.equipos[parseInt(id)]

		this.db.eliminarEquipo(id)

		this.cargarEquiposA(dataEquipo, this.equipos)
	}

	cargarJugadores(data_jugadores, jugadores) {
		data_jugadores.forEach((element) => {
			let jugador = new Jugador(element.nombre, element.id, element.documento, element.fechanac, element.domicilio)

			jugadores[element.id] = jugador
		})

		self.jugadores_cargados = true
	}

	cargarEquipos(data_equipos, equipos, jugadores) {
		data_equipos.forEach((element) => {
			let equipo = new Equipo(element.nombre)

			equipo.setId(element.id)

			if (element.jugadores.length != 0) {
				let split_jugadores = element.jugadores.split(",")

				let jugadores_equipo = equipo.getJugadores()

				split_jugadores.forEach((elem) => {
					jugadores_equipo[parseInt(elem)] = jugadores[parseInt(elem)]
					console.log(jugadores[parseInt(elem)])
				})

				console.log(jugadores_equipo)
			}

			equipo.setPj(element.pj)
			equipo.setPg(element.pg)
			equipo.setPe(element.pe)
			equipo.setPp(element.pp)
			equipo.setGf(element.gf)
			equipo.setGc(element.gc)

			equipos[element.id] = equipo
		})
	}

	async cargarDatos(db, this_) {
		await db.todosJugadores(this_.cargarJugadores, this_.jugadores)
		await db.todosEquipos(this_.cargarEquipos, this_.equipos, this_.jugadores)
	}

	cargarJugadoresA(arreglo, jugadores) {
		for (let jugador in jugadores) {
			let data = {}

			data["id"] = jugadores[jugador].getId()
			data["nombre"] = jugadores[jugador].getNombre()
			data["dni"] = jugadores[jugador].getDocumento()
			data["fecha"] = jugadores[jugador].getFechaNac()
			data["domicilio"] = jugadores[jugador].getDomicilio()

			arreglo.push(data)
		}
	}

	cargarEquiposA(arreglo, equipos) {
		for (let i in equipos) {
			let data = {}

			data["id"] = equipos[i].getId()
			data["nombre"] = equipos[i].getNombre()

			arreglo.push(data)
		}
	}

	guardarEnEquipo(jugador, idEquipo, equipos, callbackCargarJugadores, dataJugadores, db) {
		equipos[idEquipo].getJugadores()[jugador.getId()] = jugador

		equipos[idEquipo].guardar(db)

		callbackCargarJugadores(dataJugadores, equipos[idEquipo].getJugadores())
	}
}
