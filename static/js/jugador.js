class Jugador {
	constructor(nombre, id = 0, documento = null, fechanac = null, domicilio = null) {
		this.id = id
		this.nombre = nombre
		this.documento = documento
		this.fechanac = fechanac
		this.domicilio = domicilio
	}

	getDocumento() {
		return this.documento
	}

	getDomicilio() {
		return this.domicilio
	}

	getFechaNac() {
		return this.fechanac
	}

	getId() {
		return this.id
	}

	getNombre() {
		return this.nombre
	}

	setDocumento(documento) {
		this.documento = documento
	}

	setDomicilio(domicilio) {
		this.domicilio = domicilio
	}

	setFechaNac(fechanac) {
		this.fechanac = fechanac
	}

	setId(id) {
		this.id = id
	}

	setNombre(nombre) {
		this.nombre = nombre
	}

	guardar(db, jugadores, idEquipo, callbackAgregarAEquipo, equipos, callbackCargarJugadoresA, dataJugadores) {
		db.guardarJugador(this, jugadores, idEquipo, callbackAgregarAEquipo, equipos, callbackCargarJugadoresA, dataJugadores)
	}
}
