System.register(["./dexie.js", "./jugadorImp.js", "./equipoImp.js"], function (exports_1, context_1) {
    "use strict";
    var __extends = (this && this.__extends) || (function () {
        var extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return function (d, b) {
            extendStatics(d, b);
            function __() { this.constructor = d; }
            d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
        };
    })();
    var dexie_js_1, jugadorImp_js_1, equipoImp_js_1, DB_NAME, DB_VERSION, DB;
    var __moduleName = context_1 && context_1.id;
    return {
        setters: [
            function (dexie_js_1_1) {
                dexie_js_1 = dexie_js_1_1;
            },
            function (jugadorImp_js_1_1) {
                jugadorImp_js_1 = jugadorImp_js_1_1;
            },
            function (equipoImp_js_1_1) {
                equipoImp_js_1 = equipoImp_js_1_1;
            }
        ],
        execute: function () {
            DB_NAME = "dataFutbol";
            DB_VERSION = 1;
            DB = /** @class */ (function (_super) {
                __extends(DB, _super);
                function DB() {
                    var _this = _super.call(this, DB_NAME) || this;
                    _this.version(DB_VERSION).stores({
                        equipos: '++id',
                        jugadores: '++id'
                    });
                    _this.equipos.mapToClass(equipoImp_js_1.default);
                    _this.jugadores.mapToClass(jugadorImp_js_1.default);
                    return _this;
                }
                DB.prototype.todosEquipos = function () {
                    return this.equipos;
                };
                DB.prototype.todosJugadores = function () {
                    return this.jugadores;
                };
                DB.prototype.guardarJugador = function (jugador) {
                    if (jugador.id == 0) {
                        this.table("jugadores").put(jugador);
                    }
                    else {
                        this.table("jugadores").update(jugador.id, jugador);
                    }
                };
                DB.prototype.guardarEquipo = function (equipo) {
                    if (equipo.id == 0) {
                        return this.equipos.put(equipo);
                    }
                    else {
                        return this.equipos.update(equipo.id, equipo);
                    }
                };
                DB.prototype.jugador = function (id) {
                    return this.jugadores.get(id);
                };
                DB.prototype.eliminarEquipo = function (equipo) {
                    this.equipos.delete(equipo.id);
                };
                return DB;
            }(dexie_js_1.default));
            exports_1("default", DB);
        }
    };
});
