System.register([], function (exports_1, context_1) {
    "use strict";
    var FechaNac;
    var __moduleName = context_1 && context_1.id;
    return {
        setters: [],
        execute: function () {
            FechaNac = /** @class */ (function () {
                function FechaNac() {
                }
                Object.defineProperty(FechaNac.prototype, "dia", {
                    get: function () {
                        return this._dia;
                    },
                    set: function (dia) {
                        this._dia = dia;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(FechaNac.prototype, "mes", {
                    get: function () {
                        return this._mes;
                    },
                    set: function (mes) {
                        this._mes = mes;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(FechaNac.prototype, "ano", {
                    get: function () {
                        return this._ano;
                    },
                    set: function (ano) {
                        this._ano = ano;
                    },
                    enumerable: true,
                    configurable: true
                });
                return FechaNac;
            }());
            exports_1("default", FechaNac);
        }
    };
});
