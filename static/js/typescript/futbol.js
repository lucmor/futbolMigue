System.register(["./db.js", "./equipoImp.js"], function (exports_1, context_1) {
    "use strict";
    var db_js_1, equipoImp_js_1, Futbol;
    var __moduleName = context_1 && context_1.id;
    return {
        setters: [
            function (db_js_1_1) {
                db_js_1 = db_js_1_1;
            },
            function (equipoImp_js_1_1) {
                equipoImp_js_1 = equipoImp_js_1_1;
            }
        ],
        execute: function () {
            Futbol = /** @class */ (function () {
                function Futbol() {
                    this._jugadores = [];
                    this._equipos = [];
                    this.db = new db_js_1.default();
                }
                Object.defineProperty(Futbol.prototype, "jugadores", {
                    get: function () {
                        return this._jugadores;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(Futbol.prototype, "equipos", {
                    get: function () {
                        return this._equipos;
                    },
                    enumerable: true,
                    configurable: true
                });
                Futbol.prototype.jugador = function (id) {
                    var jugadorDevolver;
                    this.db.jugador(id).then(function (jugador) {
                        jugadorDevolver = jugador;
                    });
                    return jugadorDevolver;
                };
                Futbol.prototype.equipo = function (id) {
                    var encontre = false;
                    var equipoDevolver;
                    for (var i = 0; i < this._equipos.length && !encontre; i++) {
                        var equipo = this._equipos[i];
                        if (equipo.id == id) {
                            encontre = true;
                            equipoDevolver = this._equipos[i];
                        }
                    }
                    return equipoDevolver;
                };
                Futbol.prototype.agregarEquipo = function (nombre) {
                    var equipo = new equipoImp_js_1.default(nombre);
                };
                return Futbol;
            }());
        }
    };
});
