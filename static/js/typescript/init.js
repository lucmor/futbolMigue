System.register(["./db.js"], function (exports_1, context_1) {
    "use strict";
    var db_js_1, db;
    var __moduleName = context_1 && context_1.id;
    return {
        setters: [
            function (db_js_1_1) {
                db_js_1 = db_js_1_1;
            }
        ],
        execute: function () {
            db = new db_js_1.default();
            db.todosEquipos();
        }
    };
});
