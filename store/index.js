import Vuex from 'vuex'

const createStore = () => {
    return new Vuex.Store({
        state: {
			agregarEquipo: false,
			modificarEquipo: false,
			eliminarEquipo: false,
			dataModal: '',
			equipos: [],
			futbol: null
        },
        mutations: {
            setAgregarEquipo(state) {
				state.agregarEquipo = true
			},
			setModificarEquipo(state) {
				state.modificarEquipo = true
			},
			setEliminarEquipo(state) {
				state.eliminarEquipo = true
			},
			vaciarModalEquipo(state) {
				state.agregarEquipo = false
				state.modificarEquipo = false
				state.eliminarEquipo = false
			},
			setDataModal(state, data) {
				state.dataModal = data
			},
			addEquipo(state, dataEquipo) {
				state.futbol.agregarEquipo(state.dataModal, dataEquipo)
				state.dataModal = ''
			},
			modEquipo(state, dataEquipo) {
				var data = state.dataModal.split(",")

                state.futbol.modificarEquipo(data[0], data[1], dataEquipo) // index, nombreNuevo

				state.dataModal = ''
			},
			remEquipo(state, dataEquipo) {
                state.futbol.eliminarEquipo(state.dataModal, dataEquipo)

				state.dataModal = ''
			},
			setFutbol(state, futbol) {
				state.futbol = futbol
			}
        }
    })
}

export default createStore
