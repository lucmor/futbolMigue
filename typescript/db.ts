import Dexie from 'dexie.js';
import Jugador from './jugador.js';
import Equipo from './equipo.js';

const DB_NAME = "dataFutbol";
const DB_VERSION = 1;

export default class DB extends Dexie {
	private equipos: Dexie.Table<Equipo, number>
	private jugadores: Dexie.Table<Jugador, number>

	constructor() {
		super(DB_NAME);

		this.version(DB_VERSION).stores({
			equipos: '++id',
			jugadores: '++id'
		});

		this.equipos.mapToClass(Equipo);
		this.jugadores.mapToClass(Jugador);
	}

	todosEquipos(): Dexie.Table<Equipo, number> {
		return this.equipos;
	}

	todosJugadores(): Dexie.Table<Jugador, number> {
		return this.jugadores;
	}

	guardarJugador(jugador: Jugador): Promise<number> {
		if (jugador.id == 0) {
			return this.table("jugadores").put(jugador);
		} else {
			return this.table("jugadores").update(jugador.id, jugador);
		}
	}

	guardarEquipo(equipo: Equipo): Promise<number> {
		if (equipo.id == 0) {
			return this.equipos.put(equipo);
		} else {
			return this.equipos.update(equipo.id, equipo);
		}
	}

	jugador(id: number): Promise<Jugador> {
		return this.jugadores.get(id);
	}

	eliminarEquipo(equipo: Equipo) {
		this.equipos.delete(equipo.id);
	}
}
