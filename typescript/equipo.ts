import Jugador from './jugador.js'

export default class Equipo {
	private _id: number;
	private _nombre: string;
	private _jugadores: Array<Jugador>;
	private _pj: number;
	private _pg: number;
	private _pe: number;
	private _pp: number;
	private _gf: number;
	private _gc: number;

	constructor(nombre: string, id: number = 0) {
		this._id = id;
		this._nombre = nombre;
		this._jugadores = [];
		this._pj = 0;
		this._pg = 0;
		this._pe = 0;
		this._pp = 0;
		this._gf = 0;
		this._gc = 0;
	}

	get id(): number {
		return this._id;
	}

	get nombre(): string {
		return this._nombre;
	}

	get jugadores(): Jugador[] {
		return this._jugadores;
	}

	get pj(): number {
		return this._pj;
	}

	get pg(): number {
		return this._pg;
	}

	get pe(): number {
		return this._pe;
	}

	get pp(): number {
		return this._pp;
	}

	get gf(): number {
		return this._gf;
	}

	get gc(): number {
		return this._gc;
	}

	set id(id: number) {
		this._id = id;
	}

	set nombre(nombre: string) {
		this._nombre = nombre;
	}

	set pj(pj: number) {
		this._pj = pj;
	}

	set pg(pg: number) {
		this._pg = pg;
	}

	set pe(pe: number) {
		this._pe = pe;
	}

	set pp(pp: number) {
		this._pp = pp;
	}

	set gf(gf: number) {
		this._gf = gf;
	}

	set gc(gc: number) {
		this._gc = gc;
	}
}