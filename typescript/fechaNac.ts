export default class FechaNac {
	private _dia : number;
	private _mes : number;
	private _ano : number;
	
	public get dia(): number{
		return this._dia;
	}

	public get mes(): number {
		return this._mes;
	}

	public get ano(): number {
		return this._ano;
	}

	public set mes(mes: number) {
		this._mes = mes;
	}

	public set dia(dia: number) {
		this._dia = dia;
	}

	public set ano(ano: number) {
		this._ano = ano;
	}
}