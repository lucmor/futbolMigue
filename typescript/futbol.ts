import DB from './db.js';
import Jugador from './jugadorImp.js'
import Equipo from './equipoImp.js'

class Futbol {
	private db: DB;

	constructor() {
		this._jugadores = [];
		this._equipos = [];
		this.db = new DB();
	}

	get jugadores(): Jugador[] {
		return this._jugadores;
	}

	get equipos(): Equipo[] {
		return this._equipos;
	}

	jugador(id: number): Jugador {
		let jugadorDevolver: Jugador;

		this.db.jugador(id).then(jugador => {
			jugadorDevolver = jugador;
		});

		return jugadorDevolver;
	}

	equipo(id: number): Equipo {
		let encontre: boolean = false;
		let equipoDevolver: Equipo;

		for (let i: number = 0; i < this._equipos.length && !encontre; i++) {
			let equipo: Equipo = this._equipos[i];

			if (equipo.id == id) {
				encontre = true;
				equipoDevolver = this._equipos[i];
			}
		}

		return equipoDevolver;
	}

	agregarEquipo(nombre: string) {
		let equipo: Equipo = new Equipo(nombre);

		this.db.guardarEquipo(equipo).then(function() {

		})
	}
}