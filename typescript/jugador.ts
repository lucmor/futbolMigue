import FechaNac from './fechaNac.js'

export default class Jugador {
	private _id: number;
	private _nombre: string;
	private _documento: number;
	private _fechaNac: FechaNac;
	private _domicilio: string;

	constructor(nombre, id, documento, fechaNac, domicilio) {
		this._id = id;
		this._nombre = nombre;
		this._documento = documento;
		this._fechaNac = fechaNac
	}

	get id(): number {
		return this._id;
	}

	get nombre(): string {
		return this._nombre;
	}

	get documento(): number {
		return this._documento;
	}

	get fechaNac(): FechaNac {
		return this._fechaNac;
	}

	get domicilio(): string {
		return this._domicilio;
	}

	set id(id: number) {
		this._id = id;
	}

	set nombre(nombre: string) {
		this._nombre = nombre;
	}

	set documento(documento: number) {
		this._documento = documento;
	}

	set fechaNac(fechaNac: FechaNac) {
		this._fechaNac = fechaNac;
	}

	set domicilio(domicilio: string) {
		this._domicilio = domicilio;
	}
}